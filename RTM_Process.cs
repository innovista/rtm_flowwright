﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using cDevWorkflow.cDevDecisionEngine;
using cDevWorkflow.cDevErrorProvider;
using cDevWorkflow.deDataAccess;
using ConfigManager;

using Innovista.RealTimeMessaging;
using Innovista.RealTimeMessaging.Engine;

namespace IHS_RTM_Custom
{
    [stepData("RealTtimeMessage_Process", "(Near) Real-Time EDI Messaging Processing for select providers", "Innovista", "Innovista")]

    //Config variable inputs
    [stepInput("CompanyLOBID", "Company / LOB ID", true, "string")]
    [stepInput("instanceID", "Workflow Instance ID", true, "string")]
    [stepInput("ProcessStep", "Process Step to run", true, "string")]
    [stepInput("Environment", "Enter TEST or PROD for the processing environment", true, "string")]
    [stepInput("Connection", "Select the cdevdatabase connection to use", false, "selectConnectString")]
    [stepInput("ConnectionString", "Enter the DB connection string to use", false, "string")]
    [stepInput("PasswordToEncrypt", "CompanyLOBID Password to encrypt", false, "string")]
    [stepInput("RetryCount", "Retry count for restartable errors.", false, "string")]
    
    [stepReturn("True", "Successful")]
    [stepReturn("False", "Failed")]

    public class RTM_Process : deIStep
    {
        public clsStepReturn execute(clsEngineContext oEngineContext)
        {
            clsStepReturn oRet = new clsStepReturn("False", stepState.notCompleted);
            clsVariable oVarLogMessage = oEngineContext.variables["LogMessage"];
            if (oVarLogMessage is null)
            {
                oRet.returnVal = "False";
                oEngineContext.writeError("Export Payspan Claims File", $"LogMessage is a required variable. Please add to definition.", "", priorityLevel.medium);                
                oRet.stepState = stepState.notCompleted;
                GC.Collect();
                return (oRet);
            }

            //set up DLL inputs
            try
            {
                oEngineContext.logMessage(entryType.info, "execute", "Inside Initial Try");
                ProgramOptions runOptions = new ProgramOptions();
                runOptions.cDevInstanceID = oEngineContext.getProcessedProperty("instanceID");
                if (!String.IsNullOrEmpty(oEngineContext.getProcessedProperty("Connection")))
                    runOptions.ConnectionString = oEngineContext.getProcessedProperty("Connection");
                else if (!String.IsNullOrEmpty(oEngineContext.getProcessedProperty("ConnectionString")))
                    runOptions.ConnectionString = oEngineContext.getProcessedProperty("ConnectionString");
                runOptions.inputProcessStatus = oEngineContext.getProcessedProperty("ProcessStep");
                runOptions.RunMode = oEngineContext.getProcessedProperty("Environment");
                runOptions.CompanyLOBID = oEngineContext.getProcessedProperty("CompanyLOBID");
                runOptions.PasswordToEncrypt = oEngineContext.getProcessedProperty("PasswordToEncrypt");

                int tempCount = 0;
                if (!String.IsNullOrEmpty(oEngineContext.getProcessedProperty("RetryCount")))
                    if (int.TryParse(oEngineContext.getProcessedProperty("RetryCount"), out tempCount))
                        runOptions.RetryCount = tempCount;

                oEngineContext.logMessage(entryType.info, "execute", "Run Options Loaded");
                oEngineContext.logMessage(entryType.info, "execute", "Connection String: " + runOptions.ConnectionString);

                RealTimeMessageProcessing oProcessEngine = new RealTimeMessageProcessing(runOptions);

                if (!String.IsNullOrEmpty(runOptions.PasswordToEncrypt))
                    oProcessEngine.EncryptPassword();

                bool bRetVal = oProcessEngine.Process();
                oRet.stepState = stepState.complete;
                if (bRetVal)
                {
                    oRet.stepState = stepState.complete;
                    oRet.returnVal = "True";
                }                    
                else
                {
                    oRet.stepState = stepState.error;
                    oRet.returnVal = "False";
                }
            }
            catch (Exception ex)
            {
                oEngineContext.logMessage(entryType.error, "execute", ex.Message);
                oEngineContext.writeError(ex.Message,ex, priorityLevel.medium);
                oRet.stepState = stepState.error;
                oRet.returnVal = "False";
            }
            return oRet;
        }
    }
}
